const express = require('express');

function suma(req, res, next) {
    var n1 = req.params.n1;
    var n2 = req.params.n2;
    var equals = parseFloat(n1) + parseFloat(n2);
    res.send(`El resultado es ${equals}`);
}

function multiplicar(req, res, next) {
    var n1 = req.params.n1;
    var n2 = req.params.n2;
    var equals = parseFloat(n1) * parseFloat(n2);
    res.send(`El resultado es ${equals}`);
}

function dividir(req, res, next) {
    var n1 = req.params.n1;
    var n2 = req.params.n2;
    var equals = parseFloat(n1) / parseFloat(n2);
    res.send(`El resultado es ${equals}`);
}

function potencia(req, res, next) {
    var n1 = req.params.n1;
    var n2 = req.params.n2;
    var equals = Math.pow(parseFloat(n1),parseFloat(n2));
    res.send(`El resultado es ${equals}`);
}

function restar(req, res, next) {
    var n1 = req.params.n1;
    var n2 = req.params.n2;
    var equals = parseFloat(n1) - parseFloat(n2);
    res.send(`El resultado es ${equals}`);
}

module.exports = {
    suma,
    multiplicar,
    dividir,
    potencia,
    restar
}
