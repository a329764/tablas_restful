var express = require('express');
var router = express.Router();

const controller = require('../controllers/results');
/* GET users listing. */

router.get('/:n1/:n2', controller.suma);

router.post('/:n1/:n2', controller.multiplicar);

router.put('/:n1/:n2', controller.dividir);

router.patch('/:n1/:n2', controller.potencia);

router.delete('/:n1/:n2', controller.restar);

module.exports = router;
